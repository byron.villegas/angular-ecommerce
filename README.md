# Angular Ecommerce

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.

## Links
https://itnext.io/choosing-a-highly-scalable-folder-structure-in-angular-d987de65ec7

https://medium.com/geekculture/setting-up-a-mock-backend-with-angular-13-applications-26a21788f7da

https://medium.com/@mwaysolutions/10-best-practices-for-better-restful-api-cbe81b06f291

https://medium.com/@DheerajJaiswal6/angular7-with-stubby-6693d1309098

https://github.com/gothinkster/angular-realworld-example-app

https://www.coreycleary.me/project-structure-for-an-express-rest-api-when-there-is-no-standard-way

https://www.cometchat.com/tutorials/how-to-build-a-chat-app-with-websockets-and-node-js

https://github.com/goldbergyoni/nodebestpractices#1-project-structure-practices

https://scoutapm.com/blog/express-error-handling

https://github.com/icon11-community/Folder11-Ico/tree/main/ico

https://github.com/ChiragRupani/karma-chromiumedge-launcher

https://dev.to/this-is-angular/testing-angular-route-guards-with-the-routertestingmodule-45c9

https://www.embraceit.nl/blogs/how-to-test-angular-response-interceptors/

https://docs.cypress.io/guides/tooling/reporters#Examples

https://medium.com/dot-debug/running-chrome-in-a-docker-container-a55e7f4da4a8

https://purple.telstra.com/blog/run-angular-unit-tests-in-gitlab-ci

https://itnext.io/how-to-deploy-angular-application-to-heroku-1d56e09c5147

https://devcenter.heroku.com/articles/deploying-spring-boot-apps-to-heroku

https://www.gather.town/

http://profesores.fi-b.unam.mx/carlos/java/java_basico2_3.html

http://mundogeek.net/archivos/2009/03/30/modificadores-en-java/#:~:text=Los%20modificadores%20de%20acceso%2C%20como,como%20default%20o%20package%2Dprivate.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
