import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryViewComponent } from './components/category-view/category-view.component';

const routes: Routes = [
  {
    path: ':id',
    component: CategoryViewComponent,
    children: []
  },
  {
    path: ':id/:idSecondary',
    component: CategoryViewComponent,
    children: []
  },
  {
    path: ':id/:idSecondary/:idThird',
    component: CategoryViewComponent,
    children: []
  },
  {
    path: ':id/:idSecondary/:idThird/:idFour',
    component: CategoryViewComponent,
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }