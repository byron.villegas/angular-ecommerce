import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Categoria } from 'src/app/core/models/categoria';
import { Producto } from 'src/app/core/models/producto';
import { CategoriaService } from 'src/app/core/services/categoria.service';
import { ProductoService } from 'src/app/core/services/producto.service';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.css']
})
export class CategoryViewComponent implements OnInit {
  id: number = 0;
  idSecondary: number = 0;
  idThird: number = 0;
  idFour: number = 0;
  primeraCategoria: Categoria = { id: 0, idPadre: 0, nombre: '', ruta: '', imagen: '', subCategorias: [] };
  segundaCategoria: Categoria = { id: 0, idPadre: 0, nombre: '', ruta: '', imagen: '', subCategorias: [] };
  terceraCategoria: Categoria = { id: 0, idPadre: 0, nombre: '', ruta: '', imagen: '', subCategorias: [] };
  cuartaCategoria: Categoria = { id: 0, idPadre: 0, nombre: '', ruta: '', imagen: '', subCategorias: [] };
  productos: Producto[] = [];
  screenWidth: number = 0;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private categoriaService: CategoriaService, private productoService: ProductoService) {
    this.activatedRoute.params.subscribe(params => {
      this.id = +params['id'];
      this.idSecondary = +params['idSecondary'];
      this.idThird = +params['idThird'];
      this.idFour = +params['idFour'];

      const idCategoria = this.id && !this.idSecondary ? this.id : this.idSecondary && !this.idThird ? this.idSecondary : this.idThird && !this.idFour ? this.idThird : this.idFour;

      this.categoriaService.findCategoriaById(idCategoria).subscribe(categoria => {
        this.primeraCategoria = categoria;
        categoria.subCategorias.forEach(segCategoria => { // Segunda
          if (segCategoria.id === idCategoria)
            this.segundaCategoria = segCategoria;
          segCategoria.subCategorias.forEach(terCategoria => { // Tercera
            if (terCategoria.id === idCategoria) {
              this.segundaCategoria = segCategoria;
              this.terceraCategoria = terCategoria;
            }
            terCategoria.subCategorias.forEach(cuarCategoria => { // Cuarta
              if (cuarCategoria.id === idCategoria) {
                this.segundaCategoria = segCategoria;
                this.terceraCategoria = terCategoria;
                this.cuartaCategoria = cuarCategoria;
              }
            });
          });
        });
      });
      this.productoService.findProductosByCategoria(idCategoria).subscribe(productos => {
        this.productos = productos;
      });
    });
  }

  ngOnInit() {
    this.screenWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.screenWidth = window.innerWidth;
  }

  viewProduct(id: number) {
    this.router.navigate(['product', id]);
  }
}