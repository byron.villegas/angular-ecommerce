import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Producto } from 'src/app/core/models/producto';
import { ProductoService } from 'src/app/core/services/producto.service';

@Component({
  selector: 'app-home-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  productos: Producto[] = [];
  screenWidth: number = 0;

  constructor(private router: Router, private productService: ProductoService) {
    this.productService.getProductos().subscribe(productos => {
      this.productos = productos;
    });
  }

  ngOnInit() {
    this.screenWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this.screenWidth = window.innerWidth;
  }

  viewProduct(id: number): void {
    this.router.navigate(['product', id]);
  }

  onSortChange(event: Event): void  {
    let sortProperty: string = (event.target as HTMLInputElement).value;
    this.productService.sortProductosByProperty(sortProperty).subscribe(productos => {
      this.productos = productos;
    });
  }
}