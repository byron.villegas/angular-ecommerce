import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  searchForm: FormGroup;

  constructor(private router: Router) {
    this.searchForm = new FormGroup({
      text: new FormControl('', [Validators.required, Validators.maxLength(50)])
    });
  }

  ngOnInit() { }

  get form(): FormGroup {
    return this.searchForm;
  }


  onSearch(): void {
    this.router.navigate(['/product/search'], { queryParams: { nombre: this.searchForm.controls['text'].value } });
  }
}