import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BasketListComponent } from './components/basket-list/basket-list.component';

const routes: Routes = [
  {
    path: '',
    component: BasketListComponent,
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasketRoutingModule { }