import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasketListComponent } from './components/basket-list/basket-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BasketRoutingModule } from './basket-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BasketRoutingModule
  ],
  declarations: [BasketListComponent]
})
export class BasketModule { }