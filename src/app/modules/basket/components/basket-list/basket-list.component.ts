import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/core/models/producto';

@Component({
  selector: 'app-basket-list',
  templateUrl: './basket-list.component.html',
  styleUrls: ['./basket-list.component.css']
})
export class BasketListComponent implements OnInit {
  producto: Producto = {id: 0, sku: 15207410, imagen: './assets/img/products/n64.png', nombre: 'Nintendo 64', descripcion: 'Nintendo 64', caracteristicas: [], marca: 'Nintendo', precio: 100000, idCategoria: 3}

  constructor() { }

  ngOnInit() { }
}