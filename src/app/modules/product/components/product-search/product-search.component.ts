import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Producto } from 'src/app/core/models/producto';
import { ProductoService } from 'src/app/core/services/producto.service';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.css']
})
export class ProductSearchComponent implements OnInit {
  nombre: string = "";
  productos: Producto[] = [];
  screenWidth: number = 0;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private productService: ProductoService) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.nombre = params['nombre'];
      if (this.nombre)
        this.productService.findProductosByNombre(this.nombre).subscribe(productos => {
          this.productos = productos;
        });
    });
  }

  ngOnInit() {
    this.screenWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(): void {
    this.screenWidth = window.innerWidth;
  }

  viewProduct(id: number): void {
    this.router.navigate(['product', id]);
  }
}