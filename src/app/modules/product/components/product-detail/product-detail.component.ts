import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Categoria } from 'src/app/core/models/categoria';
import { Producto } from 'src/app/core/models/producto';
import { CategoriaService } from 'src/app/core/services/categoria.service';
import { ProductoService } from 'src/app/core/services/producto.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  sku: number = 0;
  base64: SafeUrl = '';
  producto: Producto = { id: 0, sku: 0, imagen: '', nombre: '', caracteristicas: [], descripcion: '', marca: '', precio: 0, idCategoria: 0 };
  primeraCategoria: Categoria = { id: 0, idPadre: 0, nombre: '', ruta: '', imagen: '', subCategorias: [] };
  segundaCategoria: Categoria = { id: 0, idPadre: 0, nombre: '', ruta: '', imagen: '', subCategorias: [] };
  terceraCategoria: Categoria = { id: 0, idPadre: 0, nombre: '', ruta: '', imagen: '', subCategorias: [] };
  cuartaCategoria: Categoria = { id: 0, idPadre: 0, nombre: '', ruta: '', imagen: '', subCategorias: [] };

  constructor(private activatedRoute: ActivatedRoute, private sanitizer: DomSanitizer, private productoService: ProductoService, private categoriaService: CategoriaService) {
    this.activatedRoute.params.subscribe(params => {
      this.sku = +params['sku'];
      this.productoService.findProductoBySku(this.sku).subscribe(producto => {
        this.producto = producto;
        this.productoService.generateBarcodeProductoBySku(this.sku).subscribe(base64 => {
          this.base64 = this.sanitizer.bypassSecurityTrustUrl(`data:image/png;base64, ${base64}`);
        });
        if (producto.idCategoria)
          this.categoriaService.findCategoriaById(this.producto.idCategoria).subscribe(categoria => {
            this.primeraCategoria = categoria;
            categoria?.subCategorias.forEach(segCategoria => { // Segunda
              if (segCategoria.id === this.producto.idCategoria)
                this.segundaCategoria = segCategoria;
              segCategoria.subCategorias.forEach(terCategoria => { // Tercera
                if (terCategoria.id === this.producto.idCategoria) {
                  this.segundaCategoria = segCategoria;
                  this.terceraCategoria = terCategoria;
                }
                terCategoria.subCategorias.forEach(cuarCategoria => { // Cuarta
                  if (cuarCategoria.id === this.producto.idCategoria) {
                    this.segundaCategoria = segCategoria;
                    this.terceraCategoria = terCategoria;
                    this.cuartaCategoria = cuarCategoria;
                  }
                });
              });
            });
          });
      });
    });
  }

  ngOnInit() {

  }
}