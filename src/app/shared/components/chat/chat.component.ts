import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Mensaje } from 'src/app/core/models/mensaje';
import { WebSocketService } from 'src/app/core/services/websocket.service';

@Component({
  selector: 'app-shared-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  @ViewChild('mensaje', {static: true})
  mensajeElement: ElementRef | undefined;
  mensajes: Mensaje[] = [];

  constructor(private webSocketService: WebSocketService) {

  }

  ngOnInit() {
    this.webSocketService.abrirConexion();
  }

  onMessageSend(): void {
    let msg = this.mensajeElement?.nativeElement.value;
    this.webSocketService.enviarMensaje(msg);
    this.mensajes = this.webSocketService.obtenerMensajes();
    this.mensajeElement!.nativeElement.value = '';
  }
}