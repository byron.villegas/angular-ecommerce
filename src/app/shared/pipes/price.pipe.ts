import { Pipe, PipeTransform } from '@angular/core';
import { Constants } from 'src/app/core/constants/constants';

@Pipe({
  name: 'price'
})
export class PricePipe implements PipeTransform {

  transform(value: number): string {
    return `${Constants.Symbol.DOLLAR} ${value.toString().replace(Constants.Regex.PRICE_REPLACE, Constants.Symbol.DOT)}`;
  }
}