import { Directive, ElementRef, HostListener } from '@angular/core';
import { Constants } from '../../core/constants/constants';

@Directive({
  selector: '[specialCharacters]'
})
export class SpecialCharactersDirective {

  constructor(private elementRef: ElementRef) { }

  @HostListener(Constants.Event.KEYPRESS, ['$event'])
  onKeyPress(event: KeyboardEvent) {
    return new RegExp(Constants.Regex.ONLY_NORMAL_CHARACTERS).test(event.key);
  }

  @HostListener(Constants.Event.PASTE, ['$event'])
  onPaste(event: ClipboardEvent) {
    event.preventDefault(); // Frena el evento
    this.elementRef.nativeElement.value = event.clipboardData!.getData(Constants.ContentType.TEXT_PLAIN).replace(Constants.Regex.ONLY_NORMAL_CHARACTERS_REPLACE, '');
  }
}