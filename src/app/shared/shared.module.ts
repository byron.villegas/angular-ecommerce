import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PricePipe } from './pipes/price.pipe';
import { SpecialCharactersDirective } from './directives/special-characters.directive';
import { ChatComponent } from './components/chat/chat.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PricePipe, SpecialCharactersDirective, ChatComponent],
  exports: [PricePipe, SpecialCharactersDirective, ChatComponent],
  providers: [PricePipe, SpecialCharactersDirective, ChatComponent]
})
export class SharedModule { }
