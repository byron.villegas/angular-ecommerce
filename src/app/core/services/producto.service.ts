import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Producto } from '../models/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  private path: string = 'productos';

  constructor(private http: HttpClient) { }

  getProductos(): Observable<Producto[]> {
    return this.http.get<Producto[]>(`${environment.server}/${this.path}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }

  findProductoBySku(sku: number): Observable<Producto> {
    return this.http.get<Producto>(`${environment.server}/${this.path}/${sku}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }

  findProductosByNombre(nombre: string): Observable<Producto[]> {
    return this.http.get<Producto[]>(`${environment.server}/${this.path}?nombre=${nombre}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }

  findProductosByCategoria(categoria: number): Observable<Producto[]> {
    return this.http.get<Producto[]>(`${environment.server}/${this.path}?idCategoria=${categoria}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }

  generateBarcodeProductoBySku(sku: number): Observable<string> {
    return this.http.get<string>(`${environment.server}/${this.path}/${sku}/barcode`, {
      headers: new HttpHeaders({
        'Content-Type': 'text/plain',
      }),
      responseType: 'text' as 'json'
    });
  }

  sortProductosByProperty(property: string): Observable<Producto[]> {
    return this.http.get<Producto[]>(`${environment.server}/${this.path}?sort=${property}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }
}