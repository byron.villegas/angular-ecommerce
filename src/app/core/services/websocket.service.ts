import { Injectable } from '@angular/core';
import { webSocket } from 'rxjs/webSocket';
import { environment } from 'src/environments/environment';
import { Mensaje } from '../models/mensaje';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  private subject = webSocket(environment.websocket);
  private mensajes: Mensaje[] = [];

  abrirConexion() {
    this.subject.subscribe({
      next: (msg) => {this.mensajes.push({ texto: msg + '', tipo: 'servidor', fecha: (new Date()).toISOString() }); }, // Llamado cuando recibe un mensaje desde el servidor
      error: (err) => console.log('Error: ', err), // Llamado cuando el websocket detecta un error
      complete: () => console.log('Conexion Cerrada') // Llamado cuando la conexion es cerrada
    });
  }

  enviarMensaje(mensaje: string) {
    this.subject.next(mensaje);
    this.mensajes.push({ texto: mensaje, tipo: 'cliente', fecha: (new Date()).toISOString() });
  }

  cerrarConexion() {
    this.subject.complete();
  }

  obtenerMensajes(): Mensaje[] {
    return this.mensajes;
  }
}