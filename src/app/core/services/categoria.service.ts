import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Categoria } from '../models/categoria';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  private path: string = 'categorias';

constructor(private http: HttpClient) { }

findCategoriaById(id: number): Observable<Categoria> {
  return this.http.get<Categoria>(`${environment.server}/${this.path}/${id}`, {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  });
}
}