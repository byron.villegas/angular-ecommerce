export interface Categoria {
    id: number,
    idPadre: number,
    nombre: string,
    ruta: string,
    imagen: string,
    subCategorias: Categoria[]
}