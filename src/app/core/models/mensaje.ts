export interface Mensaje {
    texto: string,
    tipo: string,
    fecha: string
}