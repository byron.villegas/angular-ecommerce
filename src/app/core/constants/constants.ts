export class Constants {
    static ContentType = {
        TEXT_PLAIN: 'text/plain'
    }

    static Event = {
        KEYPRESS: 'keypress',
        PASTE: 'paste'
    }
    
    static Regex = {
        PRICE_REPLACE: /\B(?=(\d{3})+(?!\d))/g,
        ONLY_NORMAL_CHARACTERS: /^[a-zA-Z0-9 ñáéíóúÑÁÉÍÓÚ]*$/g,
        ONLY_NORMAL_CHARACTERS_REPLACE: /[^a-zA-Z0-9 ñáéíóúÑÁÉÍÓÚ]/g
    }

    static Symbol = {
        DOLLAR: '$',
        DOT: '.'
    }
}