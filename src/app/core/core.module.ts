import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ProductoService } from './services/producto.service';
import { CategoriaService } from './services/categoria.service';
import { WebSocketService } from './services/websocket.service';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  declarations: [],
  providers: [ProductoService, CategoriaService, WebSocketService]
})
export class CoreModule { }