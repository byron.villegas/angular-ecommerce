import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { WebSocketService } from './core/services/websocket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  constructor(protected router: Router) {
    
  }

  ngOnInit() {
    
  }

  get route(): Router {
    return this.router;
  }
}