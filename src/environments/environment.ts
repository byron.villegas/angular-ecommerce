export const environment = {
  production: false,
  qa: false,
  server: 'http://localhost:3000/api',
  websocket: 'ws://localhost:3001'
};