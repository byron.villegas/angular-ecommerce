const Barcode = {
    CODE_128: 'code128',
    EAN_13: 'ean13'
}

module.exports = Barcode;