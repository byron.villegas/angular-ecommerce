const WebSocketEvent = {
    CONNECTION: 'connection',
    MESSAGE: 'message'
}

module.exports = WebSocketEvent;