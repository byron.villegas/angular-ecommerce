const config = {
    server: {
        host: '/api',
        port: 3000,
        websocket: {
            port: 3001
        }
    },
    routes: {
        productos: '/productos',
        categorias: '/categorias'
    }
}

module.exports = config;