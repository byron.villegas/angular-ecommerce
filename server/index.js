const colors = require('colors');
const config = require('./configs/config');
const app = require('./app');

const PORT = process.env.PORT || config.server.port;

app.listen(PORT, () => {
    console.log(`Server listening on ${colors.cyan(`http://localhost:${PORT}${config.server.host}`)}`);
});