const Barcode = require('../constants/barcode');
const DataType = require('../constants/data-type');
const ErrorMessage = require('../constants/error-message');
const Regex = require('../constants/regex');
const Symbol = require('../constants/symbol');
const TextAlign = require('../constants/text-align');
const productos = require('../data/productos.json');
const ErrorNegocioException = require('../exception/error-negocio-exception');

const { generateBarcode } = require('../util/barcode');

const findAll = () => {
    return productos;
}

const findBySku = (sku = '') => new Promise((resolve, reject) => {
    if (!sku.match(Regex.ONLY_NUMBERS)) {
        throw new ErrorNegocioException(ErrorMessage.CODIGO_SKU_DEBE_SER_NUMERO_ENTERO, ErrorMessage.MENSAJE_SKU_DEBE_SER_NUMERO_ENTERO);
    }

    const producto = productos.find(producto => producto.sku == parseInt(sku));

    if (!producto) {
        throw new ErrorNegocioException(ErrorMessage.CODIGO_PRODUCTO_NO_ENCONTRADO, ErrorMessage.MENSAJE_PRODUCTO_NO_ENCONTRADO);
    }

    resolve(producto);
});

const findByPropertyAndValue = (property = '', value = '') => new Promise((resolve, reject) => {
    const producto = findAll()[0];
    if (!producto[property]) {
        throw new ErrorNegocioException(ErrorMessage.CODIGO_PROPIEDAD_NO_ENCONTRADA, ErrorMessage.MENSAJE_PROPIEDAD_NO_ENCONTRADA);
    }

    if ((typeof producto[property]) === DataType.NUMBER && isNaN(parseInt(value))) {
        throw new ErrorNegocioException(ErrorMessage.CODIGO_FORMATO_DE_VALOR_NO_PERMITIDO_PARA_LA_PROPIEDAD, ErrorMessage.MENSAJE_FORMATO_DE_VALOR_NO_PERMITIDO_PARA_LA_PROPIEDAD);
    }

    const productosFiltrados = productos.filter(producto => {
        if ((typeof producto[property]) === DataType.STRING)
            return producto[property].toUpperCase().includes(value.toUpperCase());
        if ((typeof producto[property]) === DataType.NUMBER && !isNaN(parseInt(value)))
            return producto[property] == value;
    });

    resolve(productosFiltrados);
});

const sortByProperty = (property = '') => new Promise((resolve, reject) => {
    const propiedad = property.includes(Symbol.MINUS) || property.includes(Symbol.PLUS) ? property.substring(1).trim() : property.trim();

    if (!productos[0][propiedad]) {
        throw new ErrorNegocioException(ErrorMessage.CODIGO_PROPIEDAD_NO_ENCONTRADA, ErrorMessage.MENSAJE_PROPIEDAD_NO_ENCONTRADA);
    }

    let sortOrder = 1;
    if (property[0] === '-')
        sortOrder = -1;

    const productosOrdenados = productos.sort((x, y) => {
        let result = (x[propiedad] < y[propiedad]) ? -1 : (x[propiedad] > y[propiedad]) ? 1 : 0;
        return result * sortOrder;
    });

    resolve(productosOrdenados);
});

const generateBarcodeBySku = (sku = '') => new Promise((resolve, reject) => {
    if (!sku.match(Regex.ONLY_NUMBERS)) {
        throw new ErrorNegocioException(ErrorMessage.CODIGO_SKU_DEBE_SER_NUMERO_ENTERO, ErrorMessage.MENSAJE_SKU_DEBE_SER_NUMERO_ENTERO);
    }

    generateBarcode(Barcode.CODE_128, sku.toString(), 3, 5, TextAlign.CENTER).then(resp => resolve(resp)).catch(error => reject(error));
});

module.exports = { findAll, findBySku, findByPropertyAndValue, sortByProperty, generateBarcodeBySku };