const ErrorMessage = require('../constants/error-message');
const Regex = require('../constants/regex');
const categorias = require('../data/categorias.json');
const ErrorNegocioException = require('../exception/error-negocio-exception');


const findAll = () => {
    return categorias;
}

const findById = (id = '') => new Promise((resolve, reject) => {
    if (!id.match(Regex.ONLY_NUMBERS)) {
        throw new ErrorNegocioException(ErrorMessage.CODIGO_IDENTIFICADOR_UNICO_DEBE_SER_NUMERO_ENTERO, ErrorMessage.MENSAJE_IDENTIFICADOR_UNICO_DEBE_SER_NUMERO_ENTERO);
    }
    let response;
    categorias.forEach(categoria => {
        if (categoria.id === parseInt(id))
            response = categoria;
        categoria.subCategorias.forEach(subCategoria => {
            if (subCategoria.id === parseInt(id))
                response = categoria;
            subCategoria.subCategorias.forEach(cat => {
                if (cat.id === parseInt(id))
                    response = categoria;
                cat.subCategorias.forEach(c => {
                    if (c.id === parseInt(id))
                        response = categoria;
                });
            });
        });
    });
    resolve(response);
});

module.exports = { findAll, findById };