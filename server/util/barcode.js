const bwipjs = require('bwip-js');

const generateBarcode = (type = '', value = '', scale = 0, height = 0, textAlign = '') => new Promise((resolve, reject) => {
    bwipjs.toBuffer({ bcid: type, text: value, scale: scale, height: height, includetext: true, textxalign: textAlign, }, (err, png) => {
        if(err) {
            reject(err);
        }
        
        resolve(png.toString('base64'));
    });
});

module.exports = { generateBarcode }