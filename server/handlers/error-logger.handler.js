const logger = require('pino')();

const errorLoggerHandler = (error, req, res, next) => {
    logger.error({type: error.constructor.name, msg: error});
    next(error);
}

module.exports = { errorLoggerHandler }