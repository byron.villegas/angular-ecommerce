const HttpStatus = require("../constants/http-status");
const ErrorNegocioException = require("../exception/error-negocio-exception");
const ErrorTecnicoException = require("../exception/error-tecnico-exception");

const errorHandler = (error, req, res, next) => {
    if (error instanceof ErrorNegocioException)
        res.status(HttpStatus.BAD_REQUEST).send(error);
    if(error instanceof ErrorTecnicoException)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(error);
}

module.exports = { errorHandler }