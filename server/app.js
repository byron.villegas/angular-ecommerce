const server = require('./configs/server.config');
const config = require('./configs/config');

const { errorHandler } = require('./handlers/error.handler');
const { errorLoggerHandler } = require('./handlers/error-logger.handler');

const WebSocketEvent = require('./constants/web-socket-event');
const { WebSocketServer } = require('ws');
const webSocketServer = new WebSocketServer({ port: config.server.websocket.port });

webSocketServer.on(WebSocketEvent.CONNECTION, ws => {
    ws.on(WebSocketEvent.MESSAGE, data => {
        let msg = data.toString().replace(/"/g, '');
        if (msg.toUpperCase() === 'CERRAR' || msg.toUpperCase() === 'SALIR') {
            ws.send('"Cerrare la conversacion..."')
            ws.send('"Conversacion cerrada"')
            ws.close();
        }
        ws.send('"Hola soy el servidor"');
    });
});

server.use(config.server.host + config.routes.categorias, require('./routes/categoria.route'));
server.use(config.server.host + config.routes.productos, require('./routes/producto.route'));

server.use(errorLoggerHandler);
server.use(errorHandler);

module.exports = server;