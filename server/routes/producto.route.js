const express = require('express');
const router = express.Router();
const { getProductos, getProductoBySku, getProductoBarcodeBySku } = require('../controllers/producto.controller');

router.get('', getProductos);
router.get('/:sku', getProductoBySku);
router.get('/:sku/barcode', getProductoBarcodeBySku)

module.exports = router;