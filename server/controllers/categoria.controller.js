const HttpStatus = require('../constants/http-status');
const { findAll, findById } = require('../services/categoria.service');

const getCategorias = (req, res, next) => {
    res.status(HttpStatus.OK).send(findAll());
}

const getCategoriaById = (req, res, next) => {
    findById(req.params.id).then(resp => res.status(HttpStatus.OK).send(resp)).catch(error => next(error));
}

module.exports = { getCategorias, getCategoriaById }