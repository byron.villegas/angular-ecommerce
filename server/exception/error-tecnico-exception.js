class ErrorTecnicoException {
    constructor(causa, codigo = '', mensaje = '') {
        Error.call(this);
        Error.captureStackTrace(this);
        this.causa = causa;
        this.codigo = codigo;
        this.mensaje = mensaje;
    }
};

ErrorTecnicoException.prototype = Object.create(Error.prototype);
ErrorTecnicoException.prototype.constructor = ErrorTecnicoException;

module.exports = ErrorTecnicoException;