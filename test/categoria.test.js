const request = require('supertest');
const config = require('../server/configs/config');
const ErrorMessage = require('../server/constants/error-message');
const HttpStatus = require('../server/constants/http-status');
const app = require('../server/app');
const categorias = require('../server/data/categorias.json');

describe('GET /categorias', () => {
    it('return list of categories', () => {
        return request(app)
            .get(config.server.host + '/categorias')
            .expect(HttpStatus.OK)
            .expect('Content-Type', /json/)
            .expect(categorias);
    });
});

describe('GET /categorias/id with id ok', () => {
    it('return the category', () => {
        const category = categorias[0];
        const id = category.id;

        return request(app)
            .get(config.server.host + '/categorias/' + id)
            .expect(HttpStatus.OK)
            .expect('Content-Type', /json/)
            .expect(category);
    });
});

describe('GET /categorias/id with word', () => {
    it('return error negocio exception', () => {
        const id = 'a';

        return request(app)
            .get(config.server.host + '/categorias/' + id)
            .expect(HttpStatus.BAD_REQUEST)
            .expect('Content-Type', /json/)
            .expect({ codigo: ErrorMessage.CODIGO_IDENTIFICADOR_UNICO_DEBE_SER_NUMERO_ENTERO, mensaje: ErrorMessage.MENSAJE_IDENTIFICADOR_UNICO_DEBE_SER_NUMERO_ENTERO });
    });
});