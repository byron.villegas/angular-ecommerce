const Barcode = require('../server/constants/barcode');
const TextAlign = require('../server/constants/text-align');
const { generateBarcode } = require('../server/util/barcode');

const expect = require('chai').expect;

describe('Use barcode to generate base64 barcode with error', () => {
    it('sku of producto ean-13 up 14 digits to generate base64 barcode', () => {
        const sku = 1000000000;
        generateBarcode(Barcode.EAN_13, sku.toString(), 3, 5, TextAlign.CENTER).then(resp => {
            console.log(resp);
        }).catch(error => {
            expect('EAN-13 must be 12 or 13 digits').equal(error.message);
        });
    });
});
