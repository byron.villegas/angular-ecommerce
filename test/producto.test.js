const request = require('supertest');
const config = require('../server/configs/config');
const Barcode = require('../server/constants/barcode');
const ErrorMessage = require('../server/constants/error-message');
const HttpStatus = require('../server/constants/http-status');
const TextAlign = require('../server/constants/text-align');
const app = require('../server/app');
const productos = require('../server/data/productos.json');
const { generateBarcode } = require('../server/util/barcode');

describe('GET /productos', () => {
    it('return list of products', () => {
        return request(app)
            .get(config.server.host + '/productos')
            .expect(HttpStatus.OK)
            .expect('Content-Type', /json/)
            .expect(productos);
    });
});

describe('GET /productos/:sku with sku ok', () => {
    it('return the product', () => {
        const producto = productos[0];
        const sku = producto.sku;
        return request(app)
            .get(config.server.host + '/productos/' + sku)
            .expect(HttpStatus.OK)
            .expect('Content-Type', /json/)
            .expect(producto);
    });
});

describe('GET /productos/:sku with word', () => {
    it('return error negocio exception', () => {
        const sku = 'a';
        return request(app)
            .get(config.server.host + '/productos/' + sku)
            .expect(HttpStatus.BAD_REQUEST)
            .expect('Content-Type', /json/)
            .expect({ codigo: ErrorMessage.CODIGO_SKU_DEBE_SER_NUMERO_ENTERO, mensaje: ErrorMessage.MENSAJE_SKU_DEBE_SER_NUMERO_ENTERO });
    });
});

describe('GET /productos/:sku with sku not exists', () => {
    it('return error negocio exception', () => {
        const sku = '1';
        return request(app)
            .get(config.server.host + '/productos/' + sku)
            .expect(HttpStatus.BAD_REQUEST)
            .expect('Content-Type', /json/)
            .expect({ codigo: ErrorMessage.CODIGO_PRODUCTO_NO_ENCONTRADO, mensaje: ErrorMessage.MENSAJE_PRODUCTO_NO_ENCONTRADO });
    });
});

describe('GET /productos/:sku/barcode with sku ok', () => {
    it('return the product barcode', () => {
        const producto = productos[0];
        const sku = producto.sku;
        const barcode = '';

        generateBarcode(Barcode.CODE_128, producto.sku.toString(), 3, 5, TextAlign.CENTER).then(resp => {
            barcode = resp;

            return request(app)
                .get(config.server.host + '/productos/' + sku + '/barcode')
                .expect(HttpStatus.OK)
                .expect('Content-Type', /text\/html; charset=utf-8/)
                .expect(barcode);
        });
    });
});

describe('GET /productos/:sku/barcode with word', () => {
    it('return error negocio exception', () => {
        const sku = 'a';

        return request(app)
            .get(config.server.host + '/productos/' + sku + '/barcode')
            .expect(HttpStatus.BAD_REQUEST)
            .expect('Content-Type', /json/)
            .expect({ codigo: ErrorMessage.CODIGO_SKU_DEBE_SER_NUMERO_ENTERO, mensaje: ErrorMessage.MENSAJE_SKU_DEBE_SER_NUMERO_ENTERO });
    });
});

describe('GET /productos?sort= by property price +', () => {
    it('return list of products sortered by price desc', () => {
        const sort = 'sort=';
        const sortProperty = '+precio';
        return request(app)
            .get(config.server.host + '/productos?' + sort + sortProperty)
            .expect(HttpStatus.OK)
            .expect('Content-Type', /json/)
            .expect(productos.sort((x, y) => x.precio - y.precio));
    });
});

describe('GET /productos?sort= by property brand -', () => {
    it('return list of products sortered by brand asc', () => {
        const sort = 'sort=';
        const sortProperty = '-marca';
        return request(app)
            .get(config.server.host + '/productos?' + sort + sortProperty)
            .expect(HttpStatus.OK)
            .expect('Content-Type', /json/)
            .expect(productos.sort((x, y) => x.marca - y.marca));
    });
});


describe('GET /productos?sort= by property no exists', () => {
    const sort = 'sort=';
    const sortProperty = 'a';
    it('return error negocio exception', () => {
        return request(app)
            .get(config.server.host + '/productos?' + sort + sortProperty)
            .expect(HttpStatus.BAD_REQUEST)
            .expect('Content-Type', /json/)
            .expect({ codigo: ErrorMessage.CODIGO_PROPIEDAD_NO_ENCONTRADA, mensaje: ErrorMessage.MENSAJE_PROPIEDAD_NO_ENCONTRADA });
    });
});

describe('GET /productos?price= filtered by property', () => {
    it('return list of products filtered by price', () => {
        const producto = productos[0];
        const filterProperty = 'precio=';
        const propertyValue = producto.precio;
        return request(app)
            .get(config.server.host + '/productos?' + filterProperty + propertyValue)
            .expect(HttpStatus.OK)
            .expect('Content-Type', /json/)
            .expect(productos.filter(pro => pro.precio === producto.precio));
    });
});

describe('GET /productos?price= filtered by property', () => {
    it('return list of products filtered by brand', () => {
        const producto = productos[0];
        const filterProperty = 'marca=';
        const propertyValue = producto.marca;
        return request(app)
            .get(config.server.host + '/productos?' + filterProperty + propertyValue)
            .expect(HttpStatus.OK)
            .expect('Content-Type', /json/)
            .expect(productos.filter(pro => pro.marca === producto.marca));
    });
});

describe('GET /productos?a= filtered by property not exists', () => {
    it('return error negocio exception', () => {
        const filterProperty = 'a=';
        const propertyValue = '';
        return request(app)
            .get(config.server.host + '/productos?' + filterProperty + propertyValue)
            .expect(HttpStatus.BAD_REQUEST)
            .expect('Content-Type', /json/)
            .expect({ codigo: ErrorMessage.CODIGO_PROPIEDAD_NO_ENCONTRADA, mensaje: ErrorMessage.MENSAJE_PROPIEDAD_NO_ENCONTRADA });
    });
});

describe('GET /productos?a= filtered by property with value with incorrect format', () => {
    it('return error negocio exception', () => {
        const filterProperty = 'precio=';
        const propertyValue = 'a';
        return request(app)
            .get(config.server.host + '/productos?' + filterProperty + propertyValue)
            .expect(HttpStatus.BAD_REQUEST)
            .expect('Content-Type', /json/)
            .expect({ codigo: ErrorMessage.CODIGO_FORMATO_DE_VALOR_NO_PERMITIDO_PARA_LA_PROPIEDAD, mensaje: ErrorMessage.MENSAJE_FORMATO_DE_VALOR_NO_PERMITIDO_PARA_LA_PROPIEDAD });
    });
});