const expect = require('chai').expect;
const WebSocket = require('ws');
const app = require('../server/app');

describe('Use websocket client to connect to websocket server and send message', () => {
    it('client send message to server', () => {
        app.listen(3000, () => {
            const client = new WebSocket('ws://localhost:3001');
            client.onopen = () => {
                client.send('Hola');
            }
            client.onmessage = (event) => {
                expect('"Hola soy el servidor"'.replace(/"/g, '')).equal(event.data.replace(/"/g, ''));
                app.close();
            }
        });
    });
});

describe('Use websocket client to connect to websocket server and send message to close connection', () => {
    it('client send message to server to close connection', () => {
        app.listen(3002, () => {
            const client = new WebSocket('ws://localhost:3001');
            client.onopen = () => {
                client.send('SALIR');
            }
            client.onmessage = (event) => {
                expect('"Conversacion cerrada"'.replace(/"/g, '')).equal(event.data.replace(/"/g, ''));
                app.close();
            }
        });
    });
});